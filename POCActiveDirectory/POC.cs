using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace POCActiveDirectory
{
    public static class POC
    {
        [FunctionName("Requester")]
        public static async Task<IActionResult> requester(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            return (ActionResult)new OkObjectResult("This is a Requester " + DateTime.Now.ToShortDateString()
                + " : " + DateTime.Now.ToShortTimeString());
        }

        [FunctionName("Manager")]
        public static async Task<IActionResult> manager(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            return (ActionResult)new OkObjectResult("This is a Policy Manager " + DateTime.Now.ToShortDateString()
               + " : " + DateTime.Now.ToShortTimeString());
        }

        [FunctionName("RequesterAndManager")]
        public static async Task<IActionResult> requestermanager(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
           ILogger log)
        {
            return (ActionResult)new OkObjectResult("This is a Requester and Policy Manager " + DateTime.Now.ToShortDateString()
               + " : " + DateTime.Now.ToShortTimeString());
        }
    }
}
